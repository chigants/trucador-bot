FROM python

COPY . /app

WORKDIR /app
RUN mkdir -p /mailbox
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "app.py" ]