#!/usr/bin/env python
#
#   Trucador Bot
#

##########################################################################
#                                Imports                                 #
##########################################################################

import docker
import logging
import os
import shutil

from zmq import Message
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters
from telegram import Update


##########################################################################
#                              Bot Settings                              #
##########################################################################

BACKEND_DIR='/mailbox'
BACKEND_IMG='registry.gitlab.com/chigants/trucador-backend'
OP_DIR='/mailbox'
VOLUME_NAME='mailbox'

START_MSG="""
¡Holi\! 😁

Envíame cualquier imagen *sin comprimir* \(como archivo\) y te la devuelvo en el formato apto para Stickers de Telegram\. Para más info 👉 /help\.
"""
HELP_MSG="""
El bot para @Stickers de Telegram te pide que envíes una imagen en formato PNG o WebP y que encaje en un recuadro de `512x512` \(el lado mayor debe ser de `512px` y el otro de `512px` o menos\)\.

Si me envías una imagen *sin comprimir* \(como archivo\), yo te convierto la imagen a PNG con el tamaño adecuado para que solo tengas que reenviar esa imagen al bot de Stickers y te la convierta en sticker estático\.
"""

NO_OUTPUT_FILE_ERR_MSG='No se ha podido realizar la conversión con el fichero seleccionado.'

##########################################################################
#                          Environment settings                          #
##########################################################################

client = docker.from_env()

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

volume = client.volumes.get(VOLUME_NAME)
volume_path = volume.attrs.get('Mountpoint')

##########################################################################
#                               Functions                                #
##########################################################################

# Create directories for conversion
def create_conversion_dirs(conversion_id):
    os.mkdir(f'{OP_DIR}/{conversion_id}')
    os.mkdir(f'{OP_DIR}/{conversion_id}/input')
    os.mkdir(f'{OP_DIR}/{conversion_id}/output')

# Remove directories for conversion
def remove_conversion_dirs(conversion_id):
    shutil.rmtree(f'{OP_DIR}/{conversion_id}')

# Download a Document file given a bot Update and Context
async def download_file(update: Update, context: ContextTypes.DEFAULT_TYPE):
    file_name = update.message.document.file_name
    file = update.message.document.file_id
    obj = await context.bot.get_file(file)
    
    await obj.download(f'{OP_DIR}/{update.update_id}/input/{file_name}')

# Launch conversion container
def run_conversion(conversion_id):
    client.containers.run(BACKEND_IMG, volumes=[f'{volume_path}/{conversion_id}:{BACKEND_DIR}'], remove=True)

# Reply conversion results
async def reply_results(update: Update, context: ContextTypes.DEFAULT_TYPE):
    output_path = os.listdir(f'{OP_DIR}/{update.update_id}/output')
    if len(output_path) > 0:
        await context.bot.send_document(
            chat_id=update.effective_chat.id,
            document=open(f'{OP_DIR}/{update.update_id}/output/{output_path[0]}', 'rb')
        )
    else:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=NO_OUTPUT_FILE_ERR_MSG)

##########################################################################
#                              Bot Handlers                              #
##########################################################################

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text=START_MSG, parse_mode='MarkdownV2')

async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text=HELP_MSG, parse_mode='MarkdownV2')

async def convert(update: Update, context: ContextTypes.DEFAULT_TYPE):
    create_conversion_dirs(update.update_id)
    await download_file(update, context)
    run_conversion(update.update_id)
    await reply_results(update, context)
    remove_conversion_dirs(update.update_id)


##########################################################################
#                               Main script                              #
##########################################################################

if __name__ == '__main__':
    bot_token = open(os.environ['API_TOKEN_FILE'], mode='r').read()
    bot = ApplicationBuilder().token(bot_token).build()
    
    start_handler = CommandHandler('start', start)
    bot.add_handler(start_handler)
    
    help_handler = CommandHandler('help', help)
    bot.add_handler(help_handler)

    convert_handler = MessageHandler(filters.ATTACHMENT, convert)
    bot.add_handler(convert_handler)

    bot.run_polling()
