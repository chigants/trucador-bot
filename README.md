Trucador Bot
=========

Envía a este bot cualquier imagen **sin comprimir** (como archivo) y te la devolverá en el formato apto para Stickers de Telegram.


Despliegue
----------

Para **iniciar** la operativa del bot:

```
docker volume create mailbox
docker-compose up --build -d
```

Para **detener** la operativa del bot:

```
docker-compose down
docker volume rm mailbox
```


Configuración
-------------

Se debe de proveer del TOKEN del Bot en un fichero tal y como se marca en el `docker-compose.yaml`, por defecto `bot_token`.